# Aplicación desarrollada con PrimeVue con VUE 3 y vite

## Pasos para ejecutarla en local

- Clonar este repositorio
- Situarse dentro del repositorio `cd solutio`
- Ejecutar `npm install` para instalar las dependencias del proyecto
- `npm run dev` como comando para arrancarlo

