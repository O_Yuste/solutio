import { createApp } from 'vue'
import './style.css'
import 'primeicons/primeicons.css';
import App from './App.vue';
import PrimeVue from 'primevue/config';
import Button from 'primevue/button'
import DataTable from 'primevue/datatable';
import Column from 'primevue/column';
import ColumnGroup from 'primevue/columngroup';     //optional for column grouping
import Row from 'primevue/row';
import router from './routes/index';
import JsonCSV from 'vue-json-csv'
import 'primevue/resources/themes/saga-blue/theme.css';
import 'primevue/resources/primevue.min.css';
import 'primeicons/primeicons.css';
import Avatar from 'primevue/avatar';
import AvatarGroup from 'primevue/avatargroup';
import ToggleButton from 'primevue/togglebutton';
import InputText from 'primevue/inputtext';
import '/node_modules/primeflex/primeflex.css'
import { createPinia } from 'pinia'
const app   = createApp( App ),
      pinia = createPinia()

app.use( pinia )
app.use( PrimeVue )
app.use( router )

app.component( 'downloadCsv', JsonCSV )
app.component( 'Button', Button )
app.component( 'DataTable', DataTable )
app.component( 'Column', Column )
app.component( 'ColumnGroup', ColumnGroup )
app.component( 'Row', Row )
app.component( 'Image', Image )
app.component( 'ToggleButton', ToggleButton )
app.component( 'InputText', InputText )
app.component( 'Avatar', Avatar )
app.component( 'AvatarGroup', AvatarGroup )

router.isReady().then( () => {
  app.mount( '#app' )
} )
