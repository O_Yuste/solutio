import { http } from '../http'
import { UserDTO } from '../http/dto/UserDTO';
import { User } from '../../domain/models/User';

export const userRepository = {
  getUsers: async () => {
    const users = await http.get<UserDTO[]>('https://randomuser.me/api/?results=100')
    return users.map((userDTO): User => ({
      gender: userDTO.gender,
      name: userDTO.name,
      email: userDTO.email,
      nat: userDTO.nat,
      registered: userDTO.registered,
      dob: userDTO.dob,
      picture:userDTO.picture,
      login:userDTO.login,
      location:userDTO.location,
      phone: userDTO.phone
    }))
  }
}