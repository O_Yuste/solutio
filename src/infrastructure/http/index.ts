const get = async <T>(url: string) => {
  const response = await fetch(url, {method: 'GET'}),
        data     = await response.json()
  return data.results as T
}
export const http = {get}