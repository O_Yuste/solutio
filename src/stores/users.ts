import { defineStore } from 'pinia';
import { userService } from '../domain/services/User.service';

export const userStore = defineStore("user", {
  state: () => ({
    counter : 0,
    users: null,
    favorites: []
  }),
  getters: {
    user: () => (state) => state.users
  },
  actions: {
    async getUsers() {
      try {
        this.users = await userService.getUsers();
      } catch (error) {
        return error
      }
    },
    findUser(counter) {
      let index = 0
      this.users.forEach((e, i) => {
        if (this.users[i].login.uuid === counter) {
          index = i
        }
      })
      return this.users[index]
    },
    getFavorites() {
      return this.favorites
    },
    addFavorites (id, favorite) {
      this.favorites.push(favorite)
    },
    removeFavorites(id) {
      let index = 0
      this.favorites.forEach((e, i)=> {
        if(e.uuid === id) {
          index = i
        }
      })
      this.favorites.splice(index, 1)
    }
  },
})