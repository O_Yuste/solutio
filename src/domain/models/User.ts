export type User = {
  gender: string,
  name: string,
  email: string,
  nat: string,
  registered: string,
  dob:string,
  login:string,
  picture:string,
  location:string,
  phone:string
}