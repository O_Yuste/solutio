import { userRepository } from '../../infrastructure/repositories/user.repository'

export const userService = {
  getUsers: () => {
    return userRepository.getUsers()
  }
}