import { createWebHistory, createRouter } from "vue-router";
import { RouteRecordRaw } from "vue-router";

const routes: Array<RouteRecordRaw> = [
  {
    path: "/",
    name: "users",
    component: () => import("../users/components/Users.vue"),
  },
  {
    path: "/user/:route",
    name: "user",
    component: () => import("../users/components/User.vue"),
  },
  {
    path: "/favorites",
    name: "export",
    component: () => import("../users/components/FavoriteUsers.vue"),
  }
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;